local Lattice = {}

local ffi = require "ffi"
--require("latticeStructFloat")
require("latticeStructDouble")

ffi.cdef[[
      void* malloc (size_t size);
      void* realloc (void* ptr, size_t size);
      void free (void* ptr);
]]

local function LogAdd(log_a, log_b)
   local maxab = math.max(log_a, log_b)
   local res = maxab + math.log(math.exp(log_a-maxab) + math.exp(log_b-maxab))
   if res ~= res then print("nan in logAdd"); exit() end
   return res
end

--optimized expm
-- local function expm(x)
--    local A0 = 1.0
--    local A1 = 0.125
--    local A2 = 0.0078125
--    local A3= 0.00032552083
--    local A4 = 1.0172526e-5

--    if x<13.0 then
--       --if(x < -0.0001)
--       --printf("GLUPS: %f\n", x);

--       local y = A0+x*(A1+x*(A2+x*(A3+x*A4)))
--       y = y*y
--       y = y*y
--       y = y*y
--       y = 1/y
--       if y~=y then print("nan in expm"); exit() end
--       return y
--    else
--       return 0
--    end
-- end

local function expm(x)
   return math.exp(-x)
end


function Lattice.iter(type)
   if type=="node" then type="LatticeNode" 
   else if type=="edge" then type="LatticeEdge" 
      else error("Error in iter : type must be 'node' or 'edge'") end
   end
   
   local nbBlock = 5
   local currentBlock = 0
   local nbPerBlock = 1000000
   local types = ffi.cast(type.."**", ffi.C.malloc(ffi.sizeof(type.."*")*nbBlock))
   types[currentBlock] = ffi.C.malloc(ffi.sizeof(type)*nbPerBlock)
   
   local count = 0
   local function get()
      local res = types[currentBlock][count]
      count = count + 1
      if count>=nbPerBlock then
	 --print("adding " .. type)
	 currentBlock = currentBlock + 1
	 if currentBlock>=nbBlock then
	    --print("adding " .. type)
	    nbBlock = nbBlock * 2
	    types = ffi.cast(type.."**", ffi.C.realloc(types, ffi.sizeof(type.."*")*nbBlock))
	    --print("block added")
	 end
	 types[currentBlock] = ffi.C.malloc(ffi.sizeof(type)*nbPerBlock)
	 --print(type .. " added")
	 count = 0
      end
      if type=="LatticeNode" then
	 res.edge = nil
	 res.max_edge = nil
      end
      return res
   end
   
   local function nb() return (currentBlock*nbPerBlock)+count end
   
   return get, nb
end


local getNode, nbNodes = Lattice.iter("node")
local getEdge, nbEdges = Lattice.iter("edge")


-------------------------------------------------------
-----------------------Functions-----------------------
-------------------------------------------------------
local latticemt = {}
function latticemt:forward(sz)
   sz = sz or self.size
   local startNode = self.size - sz
   local node = self.node[startNode]
   while node~=nil do
      node.max_edge = nil
      node.accScore = node.score[0]
      node = node.next
   end
   for i=startNode+1,self.size-1 do
      local node = self.node[i]
      while node~=nil do
	 local edge = node.edge
	 local maxedge = nil
	 local maxscore = -math.huge
	 while edge~=nil do
	    if edge.score[0] + edge.src_node.accScore > maxscore and edge.src_node.col>startNode then
	       maxscore = edge.score[0] + edge.src_node.accScore
	       maxedge = edge
	    end
	    edge = edge.next
	 end
	 node.max_edge = maxedge
	 node.accScore = node.score[0] + maxscore
	 node = node.next
      end
   end
   
   --find the best end-node
   local maxendnode = nil
   local maxscore = -math.huge
   node = self.node[self.size-1]
   while node~=nil do
      if node.accScore > maxscore then
	 maxscore = node.accScore
	 maxendnode = node
      end
      node = node.next
   end

   --return the best path
   node = maxendnode
   self.path.size = 0
   while node~=nil do
      self.path.path[self.path.size] = node
      if node.max_edge~=nil then
	 self.path.edge[self.path.size] = node.max_edge
	 node = node.max_edge.src_node
      else
	 node = nil
      end
      self.path.size = self.path.size + 1 --tester si path assez long -> realloc path.path 
   end

   --compute latticeScore
   local latticeScore = -math.huge
   node = self.node[self.size-1]
   while node~=nil do
      if node.accScore>latticeScore then latticeScore = node.accScore end
      node = node.next
   end
   return latticeScore, self.path
end
function latticemt:forward_correct_path(path)	
   local score = 0
   for i=path.size-1,0,-1 do
      score = score + path.path[i].score[0]
      if i>0 then score = score + path.edge[i-1].score[0] end
   end
   return score
end
function latticemt:forward_correct(tab)
   local size = 0
   for i=0, #tab/2-1 do size = size + tab[(i*2)+2] end
   --print("size " .. size)
   local startNode = self.size - size
   --print(startNode)
   local node
   local score = 0
   local edge
   local i = 1+startNode
   local j = tab[1]
   local previousNode = self.tabNodes[(i-1)*self.nbTag + (j-1)]
   --print(i .. " " .. j)
   --print("node " .. previousNode.col .. " " .. previousNode.idx)						    
   score = score + previousNode.score[0]
   
   for k=1,(#tab-2)/2 do
      i = i +tab[(k*2)]
      j=tab[(k*2)+1]
      node = self.tabNodes[(i-1)*self.nbTag + (j-1)]
      --print("node " .. node.col .. " " .. node.idx)
      score = score + node.score[0]
      edge = node.edge
      while edge ~= nil do
	 if edge.src_node.idx==previousNode.idx and edge.src_node.col==previousNode.col then
	    score = score + edge.score[0]
	    previousNode = node
	    break
	 end
	 edge = edge.next
      end
   end
   return score
end
function latticemt:backward(grad, sz)
   sz = sz or self.size
   local startNode = self.size - sz
   --raz
   -- for t=startNode,lattice.size-1 do
   --    node = lattice.node[t]    
   --    while node~=nil do
   -- 	  print("raz " .. node.col .. " " .. node.idx)
   
   -- 	  edge = node.edge
   -- 	  while edge~=nil do
   -- 	     edge.gradScore[0] = 0;
   -- 	     edge = edge.next;
   -- 	  end
   -- 	  node.gradScore[0] = 0;
   -- 	  node = node.next;
   --    end
   -- end
   --find max_node
   local latticeScore = -math.huge
   node = self.node[self.size-1];
   while node~=nil do
      if node.accScore > latticeScore then
	 latticeScore = node.accScore
	 max_node = node
      end
      node = node.next
   end
   node = max_node
   --backward
   while node~=nil do
      --print("back : " .. node.col .. " " .. node.idx .. " " .. node.gradScore[0])
      node.gradScore[0] = node.gradScore[0] + grad
      if node.max_edge~=nil then			  			  node.max_edge.gradScore[0] = node.max_edge.gradScore[0] + grad
	 node = node.max_edge.src_node
      else
	 node = nil
      end
   end
end
function latticemt:backward_correct_path(path, grad)
   local score = 0
   for i=path.size-1,0,-1 do
      path.path[i].gradScore[0] = path.path[i].gradScore[0] + grad
      if i>0 then path.edge[i-1].gradScore[0] = path.edge[i-1].gradScore[0] + grad end
   end
end
function latticemt:backward_correct(tab, grad)
   local size = 0
   for i=0, #tab/2-1 do size = size + tab[(i*2)+2] end
   local startNode = self.size - size
   local node
   local edge
   local i = 1+startNode; local j = tab[1]
   local previousNode = self.tabNodes[(i-1)*self.nbTag + (j-1)]
   previousNode.gradScore[0] = previousNode.gradScore[0] + grad
   
   for k=1,(#tab-2)/2 do
      i= i + tab[(k*2)]
      j=tab[(k*2)+1]
      --print(i .. " " .. j)
      node = self.tabNodes[(i-1)*self.nbTag + (j-1)]
      node.gradScore[0] = node.gradScore[0] + grad
      edge = node.edge
      while edge ~= nil do
	 if edge.src_node.idx==previousNode.idx and edge.src_node.col==previousNode.col then
	    edge.gradScore[0] = edge.gradScore[0] + grad
	    previousNode = node
	    break
	 end
	 edge = edge.next
      end
   end
end
function latticemt:random_path()
   local path = ffi.cast("LatticePath*", ffi.C.malloc(ffi.sizeof("LatticePath")))
   path.size =1
   path.maxsize = self.size
   path.path = ffi.cast("LatticeNode**", ffi.C.malloc(ffi.sizeof("LatticeNode*")*path.maxsize))
   path.edge = ffi.cast("LatticeEdge**", ffi.C.malloc(ffi.sizeof("LatticeEdge*")*path.maxsize))
   
   node = self.node[self.size-1]
   --counting nodes
   local nbNode = 0
   while node~=nil do
      nbNode = nbNode + 1
      node = node.next
   end
   --choosing a random node
   nNode = math.random(nbNode)
   node = self.node[self.size-1]
   for i=1,nNode-1 do
      node = node.next			     
   end
   path.path[0] = node
   local count = 0
   while node.col~=1 do
      --counting edges
      edge = node.edge
      local nbEdge = 0
      while edge~=nil do
	 nbEdge = nbEdge + 1
	 edge = edge.next
      end
      --choosing a random edge 
      nEdge = math.random(nbEdge)
      edge = node.edge
      for i=1,nEdge-1 do
	 edge = edge.next
      end
      path.edge[count] = edge
      count = count + 1
      path.size = path.size + 1
      node = edge.src_node
      path.path[count] = node
   end
   
   return path
end

ffi.cdef[[
float lattice_forward_logadd(LatticeNode** nodes, int latticesize, int sz);
]]
local f_torch_install = io.popen("dirname $(dirname $(which th))")
local torch_install = f_torch_install:read()
f_torch_install:close()

local clib = ffi.load(torch_install .. "/lib/liblattice.so")
function latticemt:forward_logadd(sz)
   if true then
      return clib.lattice_forward_logadd(self.node, self.size, sz or self.size)
   end
   sz = sz or self.size
   local startNode = self.size - sz   
   local node = self.node[startNode]
   while node~=nil do
      node.max_edge = nil
      node.accScore = node.score[0]
      node = node.next
   end
   NEXP = 0
   for t=startNode+1,self.size-1 do
      node = self.node[t]
      while node~=nil do
	 --finding max edge
	 local edge = node.edge
	 local max_edge = nil
	 local max_score = -math.huge;
	 while edge~=nil do
	    if edge.score[0] + edge.src_node.accScore > max_score and edge.src_node.col>startNode then
	       max_score = edge.score[0] + edge.src_node.accScore
	       max_edge = edge
	    end
	    edge = edge.next
         end
	 node.max_edge = max_edge;
	 --summing scores
	 local sum = 0
	 edge = node.edge
--	 while edge~=nil do
--	    if edge.src_node.col > startNode then
--	       sum = sum + expm((max_score - edge.src_node.accScore - edge.score[0]))
--	    end
--	    edge = edge.next
--	 end
	 sum = max_score + math.abs(sum) + node.score[0]
	 node.accScore = node.score[0] + sum
	 node = node.next
      end
   end  
   
   --find the best end-node
   local maxendnode = nil
   local maxscore = -math.huge
   node = self.node[self.size-1]
   while node~=nil do
      if node.accScore > maxscore then
	 maxscore = node.accScore
	 maxendnode = node
      end
      node = node.next
   end

   --return the best path
   node = maxendnode
   self.path.size = 0
   while node~=nil do
      self.path.path[self.path.size] = node
      if node.max_edge~=nil then
	 self.path.edge[self.path.size] = node.max_edge
	 node = node.max_edge.src_node
      else
	 node = nil
      end
      self.path.size = self.path.size + 1 --tester si path assez long -> realloc self.path.path 
   end
   
   local latticeScore = -math.huge;
   node = self.node[self.size-1];
   --    while node~=nil do
   --       latticeScore = LogAdd(latticeScore, node.accScore);
   --       node = node.next;
   --    end
   return latticeScore, self.path
end

ffi.cdef[[
void lattice_backward_logadd(LatticeNode** nodes, int latticesize, int sz, float g);
]]

function latticemt:backward_logadd(g, sz)
   if true then
      return clib.lattice_backward_logadd(self.node, self.size, sz or self.size, g)
   end
   sz = sz or self.size
   local startNode = self.size - sz --hop
   sum = 0;
   max_score = -math.huge;
   
   --finding max_score
   node = self.node[self.size-1]
   while node~=nil do
      if node.accScore>max_score then
	 max_score = node.accScore
      end
      node = node.next
   end
   
   node = self.node[self.size-1]
   while node~=nil do
      node.accGradScore = expm((max_score - node.accScore))
      if node.accGradScore~=node.accGradScore then error("error1") end
      sum = sum + node.accGradScore
      node = node.next
   end
   node = self.node[self.size-1]
   
   while node~=nil do
      node.accGradScore = node.accGradScore/sum
      if node.accGradScore~=node.accGradScore then
	 error("error1") 
      end

      node = node.next
   end
   
   --zero
   for t=self.size-1,startNode,-1 do
      node = self.node[t]
      while node~=nil do
	 edge = node.edge;
	 while edge~=nil do
	    edge.src_node.accGradScore = 0;
	    edge = edge.next
	 end
	 node = node.next;
      end
   end
   
   for t=self.size-1,startNode+1,-1 do
      node = self.node[t];
      while node~=nil do
	 --if node.idx==513 then print(node.gradScore[0] .. " " .. g*node.accGradScore) end
	 node.gradScore[0] = node.gradScore[0] + g*node.accGradScore;
	 sum = 0;

	 ------------------old version but more efficient--------------------
	 max_score = node.max_edge.score[0] + node.max_edge.src_node.accScore
	 edge = node.edge;
	 
--	 local st = ""
		 while edge~=nil do
	    if edge.src_node.col>startNode then
--	       st = st .. " " .. edge.src_node.accScore + edge.score[0] .. "(" .. edge.src_node.col .. " " .. edge.src_node.idx .. ")"
	       edge.accEdge = expm((max_score - edge.src_node.accScore - edge.score[0]));
	       sum = sum + edge.accEdge;
	    end
	    edge = edge.next;
	 end
	 
	 edge = node.edge;
	 while edge~=nil do
	    if edge.src_node.col>startNode then
	       local back = edge.accEdge
	       edge.accEdge = edge.accEdge / sum
	       --if edge.accEdge~=edge.accEdge then
	       --    error("accEdge is nan")
	       --end
	    end
	    edge = edge.next;
	 end

	 ---------------new version test-------------
	 -- edge = node.edge
	 -- while edge~=nil do
	 --    if edge.src_node.col>startNode then
	 --       local tempedge = node.edge
	 --       local sum = 0
	 --       while tempedge~=nil do
	 -- 	  if edge.src_node.idx==tempedge.src_node.idx 
	 -- 	     and edge.src_node.col==tempedge.src_node.col then
	 -- 	     --print("same")
	 -- 	  else
	 -- 	     --print("add")
	 -- 	     sum = sum + math.exp(tempedge.src_node.accScore 
	 -- 				  + tempedge.score[0] - edge.src_node.accScore - edge.score[0])
	 -- 	  end
	 -- 	  tempedge = tempedge.next
	 --       end
	 --       edge.accEdge = 1 / (1 + sum)
	 --    end
	 --    edge = edge.next
	 -- end

	 edge = node.edge;
	 while edge~=nil do
	    if edge.src_node.col>startNode then
	       edge.src_node.accGradScore = edge.src_node.accGradScore + node.accGradScore*edge.accEdge
	       if edge.src_node.accGradScore~=edge.src_node.accGradScore then error("edge.src_node.accGradScore is nan") end
	       edge.gradScore[0] = edge.gradScore[0] + edge.accEdge * g*node.accGradScore;
	    end
	    edge = edge.next;
	 end
	 node = node.next;
      end
   end
   
   node = self.node[startNode];
   while node~=nil do
      node.gradScore[0] = node.gradScore[0] + g*node.accGradScore;
      node = node.next;
   end
end	
function latticemt:nbNodes()
   return nbNodes()
end
function latticemt:nbEdges()
   return nbEdges()
end

function latticemt:toString(sz)
   sz = sz or self.size
   local startNode = self.size - sz
   
   print("size " .. self.size)
   local compteurEdge = 0
   local compteurNode = 0
   for i=1+startNode,self.size do
      --print(i-1)
      print("colonne " .. i)
      local node = self.node[i-1]
      while node~=nil do
	 compteurNode = compteurNode + 1
	 --print(node)
	 print("node " .. node.col .. " " .. node.idx .. "s(" .. node.score[0] .. ") gs(" .. node.gradScore[0] .. ")  as(" .. node.accScore .. ")")
	 local edge = node.edge
	 while edge~=nil do
	    compteurEdge = compteurEdge + 1
	    print("\t<- " .. edge.src_node.col .. " " .. edge.src_node.idx .. "(" .. edge.score[0] .. ") (" .. edge.gradScore[0] .. ")")
	    edge = edge.next
	 end
	 node = node.next
	 --print(node)
      end
   end
end

function Lattice.new(T, N, f_node, f_edge)
   --local lattice = ffi.cast("Lattice*", ffi.C.malloc(ffi.sizeof("Lattice")))
   local lattice = {}
   lattice.size = T
   lattice.nbTag = N
   local N = N
  
   lattice.node  = ffi.cast("LatticeNode**", ffi.C.malloc(ffi.sizeof("LatticeNode*")*T))
   for i=1,T do lattice.node[i-1] = nil end
   
   local tabNodes = ffi.cast("LatticeNode**", ffi.C.malloc(ffi.sizeof("LatticeNode*")*T*N))
   
   --creating nodes
   local currentNode;
   local currenti = -1
   for i, j, ptr, grad  in f_node() do
      if (i>T or j>N) then error("node indice incorrect in f_node " .. i .. " " .. j) end
      --print("nodes : " .. i .. " " .. j)
      if currenti ~= i-1 then
	 currenti = i-1
	 currentNode = getNode()
	 currentNode.col = i
	 currentNode.idx = j
	 currentNode.score = ptr
	 currentNode.gradScore = grad
	 currentNode.next = nil
	 lattice.node[currenti] = currentNode 
      else
	 local node = getNode()
	 node.col = i
	 node.idx = j
	 node.score = ptr
	 node.gradScore = grad
	 node.next = nil
	 currentNode.next = node
	 currentNode = currentNode.next
      end
      tabNodes[((i-1)*N) + (j-1)] = currentNode
   end
   
   --creating edges
   local currentEdge
   local tempEdge
   local currentNode
   for i,j,i_,j_, ptr, grad in f_edge() do
      --print(i_ .. " " .. j_ .. " " .. i .. " " .. j)
      if (i>T or j>N or i_>T or j_>N) then 
	 error("node indice incorrect in f_edge")
      end
      if currentNode~=nil and currentNode.col == i and currentNode.idx == j then
	 tempEdge = getEdge()
	 tempEdge.src_node = tabNodes[((i_-1)*N) + (j_-1)]
	 tempEdge.score = ptr
	 tempEdge.gradScore = grad
	 tempEdge.next = nil
	 tempEdge.size = i-i_
	 currentEdge.next = tempEdge
	 currentEdge = currentEdge.next
      else
	 currentNode = tabNodes[((i-1)*N) + (j-1)]
	 
	 currentEdge = getEdge()
	 --print(nbEdges())
	 --print(currentEdge)
	 currentEdge.score = ptr
	 currentEdge.gradScore = grad
	 --print(currentEdge)
	 currentEdge.src_node = tabNodes[((i_-1)*N) + (j_-1)]
	 currentEdge.next = nil
	 currentEdge.size = i-i_
	 --print(currentNode)
	 currentNode.edge = currentEdge
      end
   end
   
   lattice.tabNodes = tabNodes

   lattice.path = ffi.cast("LatticePath*", ffi.C.malloc(ffi.sizeof("LatticePath")))
   lattice.path.size =0
   lattice.path.maxsize = 10000
   lattice.path.path = ffi.cast("LatticeNode**", ffi.C.malloc(ffi.sizeof("LatticeNode*")*lattice.path.maxsize))
   lattice.path.edge = ffi.cast("LatticeEdge**", ffi.C.malloc(ffi.sizeof("LatticeEdge*")*lattice.path.maxsize))	
   
   setmetatable(lattice, {__index=latticemt})

   return lattice
end

return Lattice
