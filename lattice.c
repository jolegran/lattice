#include <math.h>
#include <stdlib.h>

/* #define USE_DOUBLE */

#ifdef USE_DOUBLE
#define real double
#else
#define real float
#endif

#define expm(x)  exp(-(x))

#ifdef USE_DOUBLE
#define MINUS_LOG_THRESHOLD -39.14
#else
#define MINUS_LOG_THRESHOLD -18.42
#endif

real LogAdd(real log_a, real log_b)
{
  real minusdif;

  if (log_a < log_b)
  {
    real tmp = log_a;
    log_a = log_b;
    log_b = tmp;
  }

  minusdif = log_b - log_a;
  if (minusdif < MINUS_LOG_THRESHOLD)
    return log_a;
  else
    return log_a + log1p(exp(minusdif));
}

typedef struct LatticeNode_
{
    struct LatticeNode_ *next;
    struct LatticeEdge_ *edge;
    struct LatticeEdge_ *max_edge;
    
    real *score;
    real *gradScore;
    real accScore;
    real accGradScore;
    
    int idx;
    int col;
} LatticeNode;

typedef struct LatticeEdge_
{
    struct LatticeEdge_ *next;
    struct LatticeNode_ *src_node;
    
    real *score;
    real *gradScore;
    real accEdge;
    int size;
    
} LatticeEdge;

real lattice_forward_logadd(LatticeNode** nodes, int latticesize, int sz)
{
  int startNode = latticesize - sz;
  LatticeNode *node = nodes[startNode];
  int t;

  while(node)
  {
    node->max_edge = NULL;
    node->accScore = node->score[0];
    node = node->next;
  }

  for(t=startNode+1; t < latticesize; t++)
  {
    node = nodes[t];
    while(node)
    {
      /* finding max edge */
      LatticeEdge *edge = node->edge;
      LatticeEdge *max_edge = NULL;
      real max_score = -HUGE_VAL;
      while(edge)
      {
        if((edge->src_node->col>startNode) && (edge->score[0] + edge->src_node->accScore > max_score))
        {
          max_score = edge->score[0] + edge->src_node->accScore;
          max_edge = edge;
        }
        edge = edge->next;
      }
      node->max_edge = max_edge;

      /* summing scores */
      real sum = 0;
      edge = node->edge;
      while(edge)
      {
        if(edge->src_node->col > startNode)
          sum = sum + expm(max_score - edge->src_node->accScore - edge->score[0]);
        edge = edge->next;
      }
      sum = max_score + log(sum) + node->score[0];
      node->accScore = sum;
      node = node->next;
    }
  }
      
  real latticeScore = -HUGE_VAL;
  node = nodes[latticesize-1];
  while(node)
  {
    latticeScore = LogAdd(latticeScore, node->accScore);
    node = node->next;
  }

  return latticeScore;
}


void lattice_backward_logadd(LatticeNode** nodes, int latticesize, int sz, real g)
{
  int startNode = latticesize - sz;
  real sum = 0;
  real max_score = -HUGE_VAL;
  LatticeNode *node;
  LatticeEdge *edge;
  int t;

  /* finding max_score */
  node = nodes[latticesize-1];
  while(node)
  {
    if(node->accScore>max_score)
      max_score = node->accScore;
    node = node->next;
  }
   
  node = nodes[latticesize-1];
  while(node)
  {
    node->accGradScore = expm(max_score - node->accScore);
    sum = sum + node->accGradScore;
    node = node->next;
  }
  node = nodes[latticesize-1];
   
  while(node)
  {
    node->accGradScore = node->accGradScore/sum;
    node = node->next;
  }
   
  /* zero */
  for(t=latticesize-1; t >= startNode; t--)
  {
    node = nodes[t];
    while(node)
    {
      edge = node->edge;
      while(edge)
      {
        edge->src_node->accGradScore = 0;
        edge = edge->next;
      }
      node = node->next;
    }
  }
   
  for(t=latticesize-1; t > startNode; t--)
  {
    node = nodes[t];
    while(node)
    {
      node->gradScore[0] = node->gradScore[0] + g*node->accGradScore;
      sum = 0;
      max_score = node->max_edge->score[0] + node->max_edge->src_node->accScore;
      edge = node->edge;
	 
      while(edge)
      {
        if(edge->src_node->col>startNode)
        {
          edge->accEdge = expm(max_score - edge->src_node->accScore - edge->score[0]);
          sum = sum + edge->accEdge;
        }
        edge = edge->next;
      }

      edge = node->edge;
      while(edge)
      {
        if(edge->src_node->col>startNode)
          edge->accEdge = edge->accEdge / sum;
        edge = edge->next;
      }

      edge = node->edge;
      while(edge)
      {
        if(edge->src_node->col>startNode)
        {
          edge->src_node->accGradScore = edge->src_node->accGradScore + node->accGradScore*edge->accEdge;
          edge->gradScore[0] = edge->gradScore[0] + edge->accEdge * g*node->accGradScore;
        }
        edge = edge->next;
      }
      node = node->next;
    }
  }
   
  node = nodes[startNode];
  while(node)
  {
    node->gradScore[0] = node->gradScore[0] + g*node->accGradScore;
    node = node->next;
  }
}
