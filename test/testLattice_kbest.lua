local ffi = require "ffi"
ffi.cdef[[
      void* malloc (size_t size);
      void* realloc (void* ptr, size_t size);
      void free (void* ptr);
]]

local Lattice = require "lattice_kbest"

math.randomseed(os.time())
require "torch"
torch.setdefaulttensortype("torch.FloatTensor")


--====================================================================--
--======================Lattice example and testing==================--
--====================================================================--


-----------------------------------------------------
-------------------Params----------------------------
-----------------------------------------------------
local tab = {1,2,3,5}--,116,422} --566
local tabTrees2 = {}
--local tab = {2,1,1} 
--local tab = {2}
local tab2 = {}
local compteur = 1
for i = 1,#tab do
   for j=1,tab[i] do
      tab2[compteur] = i
      compteur = compteur + 1
   end
end
local T = 10
local size = 0
for i=1,#tab do size = size + tab[i] end

if #tab>T then error("Max window size > T") end


-----------------------------------------------------
--------------------inputs and outputs---------------
-----------------------------------------------------
local pinputs = ffi.cast("float*", ffi.C.malloc(ffi.sizeof("float")*size*T))

local pgrads =  ffi.cast("float*", ffi.C.malloc(ffi.sizeof("float")*size*T))
local pedge = ffi.cast("float*", ffi.C.malloc(ffi.sizeof("float")*size*size*(T+1)))
local pgradedge = ffi.cast("float*", ffi.C.malloc(ffi.sizeof("float")*size*size*(T+1)))
local ZOB = size*size*(T+1)

--init
for i=1,size*T do
   pinputs[i-1] = math.random()
   pgrads[i-1] = 0
end
for i=1,size*size*T do
   pedge[i-1] = math.random()
   pgradedge[i-1] = 0
end


function getScore(i,j) 
   return pinputs[(i-1)*size + (j-1)]
end
function setScore(i,j,n)
   pinputs[(i-1)*size + (j-1)] = n
end
function zeroGrads()
   for i=1,T*size do
      pgrads[i-1] = 0
   end
   for i=1,size*size*T do
      pgradedge[i-1] = 0
   end
end

do
   local i = 0
   function getScoreEdge()
      i = i + 1
      --print("getScoreEdge")
      return pedge + (i-1), pgradedge + (i-1)
   end
   function nbEdges()
      return i
   end
end

-----------------------------------------------------
--------------------nodes and edges functions--------
-----------------------------------------------------
function f_node()
   local i = 1
   local j = 0
   return function ()
	     while true do
		--print("f_node " .. i .. " " .. j)
		j = j+1		
		if j>size then i = i+1; j = 1 end
		if i==T+1 then
		   pscore = ffi.cast("float*", ffi.C.malloc(ffi.sizeof("float")))
		   pgrad = ffi.cast("float*", ffi.C.malloc(ffi.sizeof("float")))
		   pscore[0] = 0
		   pgrad[0] = 0
		   i=i+1
		   return i-1, 1, pscore, pgrad 
		end
		if i==T+2 then 
		   return nil 
		end
		--print(i-1+tab2[j]-1)
		if i-1+tab2[j]-1<T then
		   local pscore = pinputs + (i-1)*size + (j-1)   
		   pscore[0] = (math.random()*2)-1--i+j
 		   return i,j, pscore, pgrads + (i-1)*size + (j-1)
		end
	     end
	  end
end

function f_edge()
   --print("f_edge")
   --storing all edges
   local tabEdge = ffi.cast("int*", ffi.C.malloc(ffi.sizeof("int")*size*size*T*4))
   local count = 0
   local size_max = #tab
   for i=2,T do
      for j=1,size do
	 if i-1+tab2[j]-1<T then
	    for i_ = math.max(1,i-size_max), i-1 do
	       for j_=1,size do
		  if i_+tab2[j_]==i then 
		     count = count + 1
		     tabEdge[(count-1)*4] = i; tabEdge[(count-1)*4 +1] = j
		     tabEdge[(count-1)*4 +2] = i_; tabEdge[(count-1)*4 +3] = j_		     
		  end
	       end
	    end
	 end
      end
   end  
   --end node
   for i=T-#tab,T do
      for j=1,#tab2 do
	 if i+tab2[j]==T+1 then
	    count = count+1
	    --print(i .. " " .. j .. " " .. params.sentenceSizeMax+1 .. " " .. 1)
	    tabEdge[(count-1)*4] = T+1; tabEdge[(count-1)*4 +1] = 1
	    tabEdge[(count-1)*4 +2] = i; tabEdge[(count-1)*4 +3] = j	
	 end
      end
   end

   local nbEdges = count
   count = 0
   return function ()
	     if count < nbEdges then 
		count = count + 1
		local ptr, grad = getScoreEdge()
		ptr[0] = (math.random()*2)-1
		grad[0] = 0
--		print( tabEdge[(count-1)*4], tabEdge[(count-1)*4 +1], tabEdge[(count-1)*4 +2], tabEdge[(count-1)*4 +3], ptr, grad)
		return tabEdge[(count-1)*4], tabEdge[(count-1)*4 +1], tabEdge[(count-1)*4 +2], tabEdge[(count-1)*4 +3], ptr, grad
	     else 
		return nil
	     end
	  end
end


-----------------------------------------------------
------------usefull functions------------------------
-----------------------------------------------------
function printLattice(t)
   print("size " .. t.size)
   local compteurEdge = 0
   local compteurNode = 0
   for i=1,t.size do
      --print(i-1)
      print("colonne " .. i)
      local node = t.node[i-1]
      while node~=nil do
	 compteurNode = compteurNode + 1
	 --print(node)
	 print("node " .. node.col .. " " .. node.idx .. "s(" .. node.score[0] .. ") gs(" .. node.gradScore[0] .. ")  as(" .. node.accScore .. ")")
	 local edge = node.edge
	 while edge~=nil do
	    compteurEdge = compteurEdge + 1
	    print("\t<- " .. edge.src_node.col .. " " .. edge.src_node.idx .. "(" .. edge.score[0] .. ") (" .. edge.gradScore[0] .. ")")
	    edge = edge.next
	 end
	 node = node.next
	 --print(node)
      end
   end
end

function printPath(path)
   for i=path.size-1,0,-1 do
      print(path.path[i].col .. " " .. path.path[i].idx)
   end
end

function printWeight()
   for k=1,T do 
      for l=1,size do
	 io.write(pinputs[(k-1)*size + (l-1)] .. " ")
      end
   end
end

-----------------------------------------------------
------------creating lattice------------------------
-----------------------------------------------------
print("before create")
local timer = torch.Timer()
timer:reset()
l = Lattice.new(T+1, size, f_node, f_edge)

print("creation time : " .. timer:time().real)
zeroGrads()
print("after create")
--printLattice(l)

--printLattice(l)

--l:printLattice("lattice.dot")

print("nbNodes : " .. l:nbNodes()) 
print("nbEdges : " .. l:nbEdges())

--l:forward(3)
--l:backward(1,3)
--printLattice(l)

local nbbackward = 2

-----------------------------------------------------
---testing gradient with Finite difference method----
-----------------------------------------------------
-- print("test forward-backward")
-- local epsilon = 0.0001
-- print("\tnode weight")
-- for i=1,T do
--    for j=1,size do
--       --print("-------------------- " .. i .. " " .. j)
--       local backup = pinputs[(i-1)*size + (j-1)]
--       local score1, path1 = l:forward()
--       --print("score1 : " .. score1)
--       pinputs[(i-1)*size + (j-1)] = pinputs[(i-1)*size + (j-1)] + epsilon
--       local score2, path2 = l:forward()
--       --print("score2 : " .. score2)
--       l:backward(1) 
--       local deriv = (score2 - score1) / epsilon
--       --print("grad : " .. pgrads[(i-1)*size + (j-1)])
--       --print("deriv : " .. deriv)
--       --print("error grad " .. math.abs(deriv - pgrads[(i-1)*size + (j-1)]))
--       if math.abs(deriv - pgrads[(i-1)*size + (j-1)]) > 0.0001 then error("error in grad forward-backward node") end
--       zeroGrads()
--       pinputs[(i-1)*size + (j-1)] = backup
--       --io.read()
--    end
-- end
-- print("\tedge weight")
-- for i=1,nbEdges() do
--    local backup = pedge[i-1]
--    local score1, path1 = l:forward()
--    --print("score1 : " .. score1)
--    pedge[i-1] = pedge[i-1] + epsilon
--    local score2, path2 = l:forward()
--    --print("score2 : " .. score2)
--    l:backward(1)
--    local deriv = (score2 - score1) / epsilon
--    --print("grad : " .. pgradedge[i-1])
--    --print("deriv : " .. deriv)
--    --print("error grad " .. math.abs(deriv - pgradedge[i-1]))
--    if math.abs(deriv - pgradedge[i-1]) > 0.0001 then error("error in grad forward-backward edge") end
--    zeroGrads()
--    pedge[i-1] = backup

-- end


-- print("test forward-backward(size)")
-- print("\tnode weight")
--  local epsilon = 0.00001
-- for insize=1,T do
--    for i=1,T do
--       for j=1,size do
-- 	 --print("---------------- " .. i .. " " .. j)
-- 	 local backup = pinputs[(i-1)*size + (j-1)]
-- 	 local score1, path1 = l:forward(insize)
-- 	 --print("score1 : " .. score1)
-- 	 pinputs[(i-1)*size + (j-1)] = pinputs[(i-1)*size + (j-1)] + epsilon
-- 	 local score2, path2 = l:forward(insize)
-- 	 --print("score2 : " .. score2)
-- 	 l:backward(1,insize)
-- 	 --l:backward(1,insize)
-- 	 local deriv = (score2 - score1) / epsilon
-- 	 --print("grad : " .. pgrads[(i-1)*size + (j-1)])
-- 	 --print("deriv : " .. deriv)
-- 	 --print("error grad " .. math.abs(deriv - pgrads[(i-1)*size + (j-1)]))
-- 	 if math.abs(deriv - pgrads[(i-1)*size + (j-1)]) > 0.0001 then error("error in forward-backwardLogadd node") end
-- 	 zeroGrads()
-- 	 pinputs[(i-1)*size + (j-1)] = backup
--       end
--    end
-- end
-- print("\tedge weight")
-- for insize=1,T do
--    for i=1,nbEdges() do
--       --print(insize .. " " .. i)
--       local backup = pedge[i-1]
--       local score1 = l:forward(insize)
--       --print("score1 : " .. score1)
--       pedge[i-1] = pedge[i-1] + epsilon
--       local score2 = l:forward(insize)
--       --print("toto")
--       --print("score2 : " .. score2)
--       l:backward(1, insize)
--       l:backward(1, insize)
--       local deriv = (score2 - score1) / epsilon
--       --print("grad : " .. pgradedge[i-1])
--       --print("deriv : " .. deriv)
--       --print("error grad " .. math.abs(deriv - pgradedge[i-1]))
--       if math.abs(2*deriv - pgradedge[i-1]) > 0.0001 then error("error in forward-backwardLogadd edge") end
--       zeroGrads()
--       pedge[i-1] = backup
--    end
-- end



-- print("test forward-backward_correct_path")
-- local epsilon = 0.00001
-- local path = l:random_path()
-- --printPath(path)
-- print("\tnode weight")
-- for i=1,T do
--    for j=1,size do
--       --print("---------------- " .. i .. " " .. j)
--       local backup = pinputs[(i-1)*size + (j-1)]
--       local score1 = l:forward_correct_path(path)
--       score1 = -score1
--       --print("score1 : " .. score1)
--       pinputs[(i-1)*size + (j-1)] = pinputs[(i-1)*size + (j-1)] + epsilon
--       local score2 = l:forward_correct_path(path)
--       score2 = -score2
--       --print("score2 : " .. score2)
--       l:backward_correct_path(path,-1)
--       local deriv = (score2 - score1) / epsilon
--       --print("grad : " .. pgrads[(i-1)*size + (j-1)])
--       --print("diff : " .. score2-score1)
--       --print("deriv : " .. deriv)
--       --print("error grad " .. math.abs(deriv - pgrads[(i-1)*size + (j-1)]))
--       if math.abs(deriv - pgrads[(i-1)*size + (j-1)]) > 0.0001 then error("error in forward-backward_correct_path node") end
--       zeroGrads()
--       pinputs[(i-1)*size + (j-1)] = backup
--       --io.read()
--    end
-- end
-- print("\tedge weight")
-- for i=1,nbEdges() do
--    local backup = pedge[i-1]
--    local score1 = l:forward_correct_path(path)
--    --print("score1 : " .. score1)
--    pedge[i-1] = pedge[i-1] + epsilon
--    local score2 = l:forward_correct_path(path)
--    --print("score2 : " .. score2)
--    l:backward_correct_path(path,1)
--    local deriv = (score2 - score1) / epsilon
--    --print("grad : " .. pgradedge[i-1])
--    --print("deriv : " .. deriv)
--    --print("error grad " .. math.abs(deriv - pgradedge[i-1]))
--    if math.abs(deriv - pgradedge[i-1]) > 0.0001 then error("error in forward-backward_correct_path edge") end
--    zeroGrads()
--    pedge[i-1] = backup
-- end

-- print("test forward-backward_correct")
-- local epsilon = 0.00001
-- local path = l:random_path()
-- local tabPath = {}
-- --printPath(path)
-- local sz = 1
-- for i=path.size-2,0,-1 do
--    sz = sz + path.edge[i].size
-- end
-- --print("sz : " .. sz)
-- local accsize = 0
-- local inc
-- for i=path.size-1,0,-1 do
--    table.insert(tabPath, path.path[i].idx)
--    if i-1>=0 then inc = path.edge[i-1].size else inc = sz-accsize end
--    table.insert(tabPath, inc)
--    accsize = accsize + inc
-- end
-- print("\tnode weight")
-- for i=1,T do
--    for j=1,size do
--       --print("---------------- " .. i .. " " .. j)
--       local backup = pinputs[(i-1)*size + (j-1)]
--       --print(i .. " " .. j .. " ")
--       --print(backup)
--       local score1, path1 = l:forward_correct(tabPath)
--       score1 = -score1
--       --print("score1 : " .. score1)
--       pinputs[(i-1)*size + (j-1)] = pinputs[(i-1)*size + (j-1)] + epsilon
--       local score2, path2 = l:forward_correct(tabPath)
--       score2 = -score2
--       --print("score2 : " .. score2)
--       l:backward_correct(tabPath,-1) 
--       local deriv = (score2 - score1) / epsilon
--       --print("grad : " .. pgrads[(i-1)*size + (j-1)])
--       --print("diff : " .. score2-score1)
--       --print("deriv : " .. deriv)
--       --print("error grad " .. math.abs(deriv - pgrads[(i-1)*size + (j-1)]))
--       if math.abs(deriv - pgrads[(i-1)*size + (j-1)]) > 0.0001 then error("error in forward-backward_correct node") end
--       zeroGrads()
--       pinputs[(i-1)*size + (j-1)] = backup
--    end
-- end
-- print("\tedge weight")
-- for i=1,nbEdges() do
--    local backup = pedge[i-1]
--    local score1 = l:forward_correct(tabPath)
--    --print("score1 : " .. score1)
--    pedge[i-1] = pedge[i-1] + epsilon
--    local score2 = l:forward_correct(tabPath)
--    --print("score2 : " .. score2)
--    l:backward_correct(tabPath,1)
--    local deriv = (score2 - score1) / epsilon
--    --print("grad : " .. pgradedge[i-1])
--    --print("deriv : " .. deriv)
--    --print("error grad " .. math.abs(deriv - pgradedge[i-1]))
--    if math.abs(deriv - pgradedge[i-1]) > 0.0001 then error("error in grad forward-backward_correct edge") end
--    zeroGrads()
--    pedge[i-1] = backup
-- end

-- print("test forward-backwardLogadd")
-- local epsilon = 0.0000001
-- print("\tnode weight")
-- for i=1,T do
--    for j=1,size do
--       --print("---------------- " .. i .. " " .. j)
--       local backup = pinputs[(i-1)*size + (j-1)]
--       local score1, path1 = l:forward_logadd()
--       --print("score1 : " .. score1)
--       pinputs[(i-1)*size + (j-1)] = pinputs[(i-1)*size + (j-1)] + epsilon
--       local score2, path2 = l:forward_logadd()
--       --print("score2 : " .. score2)
--       l:backward_logadd(1)
--       local deriv = (score2 - score1) / epsilon
--       --print("---")
--       --printPath(path1)
--       --print("grad : " .. pgrads[(i-1)*size + (j-1)])
--       --print("deriv : " .. deriv)
--       print("error grad " .. math.abs(deriv - pgrads[(i-1)*size + (j-1)]))
--       if math.abs(deriv - pgrads[(i-1)*size + (j-1)]) > 0.001 then 
-- 	 error("error in forward-backwardLogadd node") 	
--       end
--       zeroGrads()
--       pinputs[(i-1)*size + (j-1)] = backup
--    end
--    --io.read()
-- end
-- print("\tedge weight")
-- for i=1,nbEdges() do
--    local backup = pedge[i-1]
--    local score1 = l:forward_logadd()
--    --print("score1 : " .. score1)
--    pedge[i-1] = pedge[i-1] + epsilon
--    local score2 = l:forward_logadd()
--    --print("score2 : " .. score2)
--    l:backward_logadd(1)

--    --l:backward_logadd(1)
--    local deriv = (score2 - score1) / epsilon
--    --print("grad : " .. pgradedge[i-1])
--    --print("deriv : " .. 2*deriv)
--    --print("error grad " .. math.abs(deriv - pgradedge[i-1]))
--    if math.abs(deriv - pgradedge[i-1]) > 0.001 then error("error in forward-backwardLogadd edge") end
--    zeroGrads()
--    pedge[i-1] = backup
-- end


l:toString()
print("test forward-backwardLogadd(size)")
print("\tnode weight")
local epsilon = 0.01
for insize=4,T+1 do
   for i=1,T do
      for j=1,size do
	 --print("---------------- " .. i .. " " .. j .. " size=" .. insize)
	 local backup = pinputs[(i-1)*size + (j-1)]
	 local score1, path1 = l:forward_logadd(insize)
	 --print("score1 : " .. score1)
	 --print("path")
	 --printPath(path1)
	 --print("---")
	 pinputs[(i-1)*size + (j-1)] = pinputs[(i-1)*size + (j-1)] + epsilon
	 local score2, path2 = l:forward_logadd(insize)
	 --print("score2 : " .. score2)
	 l:backward_logadd(1,insize)
	 local deriv = (score2 - score1) / epsilon
	 --print("grad : " .. pgrads[(i-1)*size + (j-1)])
	 --print("deriv : " .. deriv)
	 print("error grad " .. math.abs(deriv - pgrads[(i-1)*size + (j-1)]))
	 if math.abs(deriv - pgrads[(i-1)*size + (j-1)]) > 0.01 then error("error in forward-backwardLogadd node") end
	 zeroGrads()
	 pinputs[(i-1)*size + (j-1)] = backup
	 --io.read()
      end
   end
end
print("\tedge weight")
for insize=1,T do
   for i=1,nbEdges() do
      
      local backup = pedge[i-1]
      local score1,path1 = l:forward_logadd(insize)
      --print("score1 : " .. score1)
      --printPath(path1)
      pedge[i-1] = pedge[i-1] + epsilon
      local score2 = l:forward_logadd(insize)
      --print("score2 : " .. score2)
      l:backward_logadd(1, insize)
      local deriv = (score2 - score1) / epsilon
      --print("grad : " .. pgradedge[i-1])
      --print("deriv : " .. deriv)
      --print("error grad " .. math.abs(deriv - pgradedge[i-1]))
      if math.abs(deriv - pgradedge[i-1]) > 0.01 then error("error in forward-backwardLogadd edge") end
      zeroGrads()
      pedge[i-1] = backup
   end
end

ffi.C.free(pinputs)
ffi.C.free(pgrads)
ffi.C.free(pedge)
ffi.C.free(pgradedge)
