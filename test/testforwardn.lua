require "TagInferenceBIOES"

torch.setdefaulttensortype('torch.FloatTensor')

torch.manualSeed(123)

local hash = {}
local list = {"AA", "BB", "CC", "DD", "EE", "FF", "GG"}--, "HH", "II", "JJ", "KK", "LL", "MM", "NN", "OO", "PP", "QQ", "RR", "SS", "TT", "UU", "VV", "WW", "XX", "YY", "ZZ"}
local idx = 1
for i=1,#list do
   hash[idx] = "B-" .. list[i] 
   idx = idx + 1
   hash[idx] = "I-" .. list[i] 
   idx = idx + 1
   hash[idx] = "E-" .. list[i] 
   idx = idx + 1
   hash[idx] = "S-" .. list[i] 
   idx = idx + 1
end
hash[idx] = "O"

for i=1,#hash do
   hash[hash[i]] = i
end

local size = 1000

tagger = nn.TagInferenceBIOES(hash, size)

tagger.trans:zero()
tagger.gradtrans:zero()

local toto = torch.rand(size,29)
--local toto = torch.Tensor({{1,2,3,4,5},{5,10,3,2,1},{5,1,2,3,4},{4,3,2,2,5}})
local T = toto:size(1)
tagger.input:narrow(1, tagger.maxT-T+1, T):copy(toto)

local kbest = 2
local timer = torch.Timer()
local scores, pathes = tagger:forward_max_k(toto, kbest)
print(timer:time().real)


for i=1, kbest do
   print("score " .. scores[i])
--    print("path " .. i .. " :")
--    print(pathes[i])
end

exit()

print("==========================================")

local kbest = 10
local scores, pathes = tagger:forward_max_k(toto, kbest)

-- for i=1, kbest do
--    print("score " .. scores[i])
--    print("path " .. i .. " :")
--    print(pathes[i])
-- end

	 
--tagger:toString()