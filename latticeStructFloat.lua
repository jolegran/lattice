local ffi = require "ffi"

ffi.cdef[[
      typedef struct LatticeNode_
      {
	 struct LatticeNode_ *next;
	 struct LatticeEdge_ *edge;
	 struct LatticeEdge_ *max_edge;
	 
	 float *score;
	 float *gradScore;
	 float accScore;
	 float accGradScore;
	 
	 int idx;
	 int col;
      } LatticeNode;
      
      typedef struct LatticeEdge_
      {
	 struct LatticeEdge_ *next;
	 struct LatticeNode_ *src_node;
	 
	 float *score;
	 float *gradScore;
         float accEdge;
	 int size;
	 
      } LatticeEdge;
      
      typedef struct LatticePath_
      {
	 int maxsize;
	 int size;
	 LatticeNode **path;
	 LatticeEdge **edge;
      } LatticePath;

      typedef struct
      {
	 LatticeNode **node;
	 int size;
      } Lattice;
]]
