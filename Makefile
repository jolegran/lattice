ifeq (, $(shell which th))
$(error torch is not installed on this computer!!!)
endif

ifndef TORCH_INSTALL
export TORCH_INSTALL=$(shell dirname $(shell dirname $(shell which th)))
endif

TORCH_SHAREDIR=$(TORCH_INSTALL)/share/lua/5.1/
TORCH_LIBDIR=$(TORCH_INSTALL)/lib

liblattice.so:
	gcc -fPIC -shared -o liblattice.so lattice.c

install: liblattice.so
	cp liblattice.so $(TORCH_LIBDIR)
	cp lattice.lua $(TORCH_SHAREDIR)
	cp latticeStructFloat.lua $(TORCH_SHAREDIR)
	cp latticeStructDouble.lua $(TORCH_SHAREDIR)
	rm liblattice.so
	@echo done
