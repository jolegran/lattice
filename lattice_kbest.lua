local Lattice = {}

local ffi = require "ffi"
require("latticeStructFloat_kbest")--jo
--require("latticeStructDouble")--jo

ffi.cdef[[
      void* malloc (size_t size);
      void* realloc (void* ptr, size_t size);
      void free (void* ptr);
]]

local function LogAdd(log_a, log_b)
   local maxab = math.max(log_a, log_b)
   local res = maxab + math.log(math.exp(log_a-maxab) + math.exp(log_b-maxab))
   if res ~= res then print("nan in logAdd"); exit() end
   return res
end

-- local function expm(x)
--    local A0 = 1.0
--    local A1 = 0.125
--    local A2 = 0.0078125
--    local A3= 0.00032552083
--    local A4 = 1.0172526e-5

--    if x<13.0 then
--       --if(x < -0.0001)
--       --printf("GLUPS: %f\n", x);

--       local y = A0+x*(A1+x*(A2+x*(A3+x*A4)))
--       y = y*y
--       y = y*y
--       y = y*y
--       y = 1/y
--       if y~=y then print("nan in expm"); exit() end
--       return y
--    else
--       return 0
--    end
-- end
local function expm(x)
   return math.exp(-x)
end


function Lattice.iter(type)
   if type=="node" then type="LatticeNode" 
   else if type=="edge" then type="LatticeEdge" 
      else error("Error in iter : type must be 'node' or 'edge'") end
   end
   
   local nbBlock = 5
   local currentBlock = 0
   local nbPerBlock = 1000000
   local types = ffi.cast(type.."**", ffi.C.malloc(ffi.sizeof(type.."*")*nbBlock))
   types[currentBlock] = ffi.C.malloc(ffi.sizeof(type)*nbPerBlock)
   
   local count = 0
   local function get()

      local res = types[currentBlock][count]
      count = count + 1
      if count>=nbPerBlock then
	 --print("adding " .. type)
	 currentBlock = currentBlock + 1
	 if currentBlock>=nbBlock then
	    --print("adding " .. type)
	    nbBlock = nbBlock * 2
	    types = ffi.cast(type.."**", ffi.C.realloc(types, ffi.sizeof(type.."*")*nbBlock))
	    --print("block added")
	 end
	 types[currentBlock] = ffi.C.malloc(ffi.sizeof(type)*nbPerBlock)
	 --print(type .. " added")
	 count = 0
      end

      if type=="LatticeNode" then
	 res.edge = nil
      end
      return res
   end

   local function nb() return (currentBlock*nbPerBlock)+count end
   
   return get, nb
end


local getNode, nbNodes = Lattice.iter("node")
local getEdge, nbEdges = Lattice.iter("edge")

function Lattice.getPFloat(i)
   local toto = ffi.cast("float*",ffi.C.malloc(ffi.sizeof("float")*i))
   for k=0,i-1 do toto[k]=-math.huge end
   return toto --a revoir pour mieux gerer la mémoire
end

function Lattice.getPInt(i)
   return ffi.C.malloc(ffi.sizeof("int")*i)--a revoir pour mieux gerer la mémoire
end

function Lattice.getPEdge(i)
   return ffi.C.malloc(ffi.sizeof("LatticeEdge*")*i)--a revoir pour mieux gerer la mémoire
end

-------------------------------------------------------
-----------------------Functions-----------------------
-------------------------------------------------------
local latticemt = {}
function latticemt:forward(sz)
   sz = sz or self.size
   local startNode = self.size - sz
   local node = self.node[startNode]

   while node~=nil do
      node.max_edge[0] = nil; 
      node.accScore[0] = node.score[0]; node = node.next
   end
   for i=startNode+1,self.size-1 do
      local node = self.node[i]
      while node~=nil do
	 local edge = node.edge
	 local maxedge = nil
	 local maxscore = -math.huge
	 while edge~=nil do
	    if edge.score[0] + edge.src_node.accScore[0] > maxscore and edge.src_node.col>startNode then
	       maxscore = edge.score[0] + edge.src_node.accScore[0]
	       maxedge = edge
	    end
	    edge = edge.next
	 end
	 node.max_edge[0] = maxedge
	 node.accScore[0] = node.score[0] + maxscore
	 node = node.next
      end
   end
   
   --find the best end-node
   local maxendnode = nil
   local maxscore = -math.huge
   node = self.node[self.size-1]
   while node~=nil do
      if node.accScore[0] > maxscore then
	 maxscore = node.accScore[0]
	 maxendnode = node
      end
      node = node.next
   end
   
   --return the best path
   node = maxendnode
   self.path.size = 0
   while node~=nil do
      self.path.path[self.path.size] = node
      if node.max_edge[0]~=nil then
	 self.path.edge[self.path.size] = node.max_edge[0]
	 node = node.max_edge[0].src_node
      else
	 node = nil
      end
      self.path.size = self.path.size + 1 --tester si path assez long -> realloc path.path 
   end
   
   --compute latticeScore
   local latticeScore = -math.huge
   node = self.node[self.size-1]
   while node~=nil do
      if node.accScore[0]>latticeScore then latticeScore = node.accScore[0] end
      node = node.next
   end
   return latticeScore, self.path
end

function latticemt:forwardkbest(sz, kbest)
   if self.kbest < kbest then
      error("not implemented yet")
   end
   
   --update lattice.kbest and realocate
   sz = sz or self.size
   local startNode = self.size - sz
   local node = self.node[startNode]
   while node~=nil do
      node.max_edge[0] = nil; node.accScore[0] = node.score[0]; 
      for i=2,kbest do node.max_edge[i-1] = nil; node.accScore[i-1] = -math.huge end
      node = node.next
   end
   
   local sc =0
   for i=startNode+1,self.size-1 do
      --print("ccolumn " .. i+1 .. "================================================")
      local node = self.node[i]
      while node~=nil do
	 --print("\tnode")
	 local edge = node.edge
	 local maxedges = {}
	 local maxedgeindices = {}
	 local maxscores = {}; for i=1, kbest do table.insert(maxscores, -math.huge) end
	 while edge~=nil do
	    --print("\t\tedge")
	    for l=1, kbest do
	       if edge.src_node.col<=startNode then print("break") break end
	       --version 1 testée : ok 
	       for m=1, kbest do
		  sc = edge.score[0] + edge.src_node.accScore[l-1] 
		  if sc > maxscores[m] then
		     table.insert(maxscores, m, sc)
		     table.insert(maxedges, m, edge)
		     table.insert(maxedgeindices, m, l)
		     --print("\t\t\tbreak")
		     break
		  end
	       end
	       -- else
	       -- 	  nbtest = nbtest + 1
	       -- 	  --print("score to insert " .. edge.score[0] + edge.src_node.accScore[l-1])
	       -- 	  if edge.score[0] + edge.src_node.accScore[l-1] <= maxscores[kbest] then
	       -- 	     nbbreak = nbbreak + 1
	       -- 	     break
	       -- 	  end
	       -- 	  local m=kbest-1
	       -- 	  while m>=1 do
	       -- 	     nbtest = nbtest + 1
	       -- 	     if edge.score[0] + edge.src_node.accScore[l-1] <= maxscores[m] then
	       -- 		break
	       -- 	     end
	       -- 	     m=m-1
	       -- 	  end	       
	       -- 	  table.insert(maxscores, m+1,edge.score[0] + edge.src_node.accScore[l-1])
	       -- 	  table.insert(maxedges, m+1, edge)
	       -- 	  table.insert(maxedgeindices, m+1, l)
	       -- end
	    end
	    edge = edge.next
	 end
	 --print(maxscores)
	 for m=1, kbest do
	    node.max_edge[m-1] = maxedges[m]
	    node.accScore[m-1] = node.score[0] + maxscores[m]
	    --print("maxedgeindices[" .. m .. "]")
	    --print(maxedgeindices[m])
	    node.max_edge_indice[m-1] = maxedgeindices[m] or -1
	 end
	 node = node.next
      end
   end
   
   --find the best end-node
   local maxendnodes = {}
   local edgeindices = {}
   local maxscores = {}; for i=1, kbest do maxscores[i] = -math.huge end
   node = self.node[self.size-1]
   while node~=nil do
      --print("node")
      --print(node.col .. " " .. node.idx)
      for i=1, kbest do
	 for j=1, kbest do
	    if node.accScore[i-1] > maxscores[j] then
	       table.insert(maxscores, j,  node.accScore[i-1])
	       table.insert(maxendnodes, j, node)
	       table.insert(edgeindices, j, i) 
	       break
	    end
	 end
      end
      node = node.next
   end

   --return the best path
   local pathes = {}
   for i=1, kbest do
      local path = {}
      node = maxendnodes[i]
      local nedge = edgeindices[i]-1
      while node~=nil do
	 table.insert(path, 1, node.idx)
	 table.insert(path, 1, node.col)
	 --print("nedge " .. nedge)
	 --print(node.col .. " " .. node.idx)
   	 if node.max_edge[0]~=nil then
   	    local backnode = node 
   	    node = node.max_edge[nedge].src_node
   	    nedge = backnode.max_edge_indice[nedge]-1
   	 else 
   	    node = nil
   	 end
      end
      table.insert(pathes, path)
   end

   return maxscores, pathes
end

function latticemt:forward_correct_path(path)	
   local score = 0
   for i=path.size-1,0,-1 do
      score = score + path.path[i].score[0]
      if i>0 then score = score + path.edge[i-1].score[0] end
   end
   return score
end
function latticemt:forward_correct(tab)
   local size = 0
   for i=0, #tab/2-1 do size = size + tab[(i*2)+2] end
   --print("size " .. size)
   local startNode = self.size - size
   --print(startNode)
   local node
   local score = 0
   local edge
   local i = 1+startNode
   local j = tab[1]
   local previousNode = self.tabNodes[(i-1)*self.nbTag + (j-1)]
   --print(i .. " " .. j)
   --print("node " .. previousNode.col .. " " .. previousNode.idx)						    
   score = score + previousNode.score[0]
   
   for k=1,(#tab-2)/2 do
      i = i +tab[(k*2)]
      j=tab[(k*2)+1]
      node = self.tabNodes[(i-1)*self.nbTag + (j-1)]
      --print("node " .. node.col .. " " .. node.idx)
      score = score + node.score[0]
      edge = node.edge
      while edge ~= nil do
	 if edge.src_node.idx==previousNode.idx and edge.src_node.col==previousNode.col then
	    score = score + edge.score[0]
	    previousNode = node
	    break
	 end
	 edge = edge.next
      end
   end
   return score
end
function latticemt:backward(grad, sz)
   sz = sz or self.size
   local startNode = self.size - sz
   --raz
   -- for t=startNode,lattice.size-1 do
   --    node = lattice.node[t]    
   --    while node~=nil do
   -- 	  print("raz " .. node.col .. " " .. node.idx)
   
   -- 	  edge = node.edge
   -- 	  while edge~=nil do
   -- 	     edge.gradScore[0] = 0;
   -- 	     edge = edge.next;
   -- 	  end
   -- 	  node.gradScore[0] = 0;
   -- 	  node = node.next;
   --    end
   -- end
   --find max_node
   local latticeScore = -math.huge
   node = self.node[self.size-1];
   while node~=nil do
      if node.accScore[0] > latticeScore then
	 latticeScore = node.accScore[0]
	 max_node = node
      end
      node = node.next
   end
   node = max_node
   --backward
   while node~=nil do
      --print("back : " .. node.col .. " " .. node.idx .. " " .. node.gradScore[0])
      node.gradScore[0] = node.gradScore[0] + grad
      if node.max_edge[0]~=nil then			  			  node.max_edge[0].gradScore[0] = node.max_edge[0].gradScore[0] + grad
	 node = node.max_edge[0].src_node
      else
	 node = nil
      end
   end
end
function latticemt:backward_correct_path(path, grad)
   local score = 0
   for i=path.size-1,0,-1 do
      path.path[i].gradScore[0] = path.path[i].gradScore[0] + grad
      if i>0 then path.edge[i-1].gradScore[0] = path.edge[i-1].gradScore[0] + grad end
   end
end
function latticemt:backward_correct(tab, grad)
   local size = 0
   for i=0, #tab/2-1 do size = size + tab[(i*2)+2] end
   local startNode = self.size - size
   local node
   local edge
   local i = 1+startNode; local j = tab[1]
   local previousNode = self.tabNodes[(i-1)*self.nbTag + (j-1)]
   previousNode.gradScore[0] = previousNode.gradScore[0] + grad
   
   for k=1,(#tab-2)/2 do
      i= i + tab[(k*2)]
      j=tab[(k*2)+1]
      --print(i .. " " .. j)
      node = self.tabNodes[(i-1)*self.nbTag + (j-1)]
      node.gradScore[0] = node.gradScore[0] + grad
      edge = node.edge
      while edge ~= nil do
	 if edge.src_node.idx==previousNode.idx and edge.src_node.col==previousNode.col then
	    edge.gradScore[0] = edge.gradScore[0] + grad
	    previousNode = node
	    break
	 end
	 edge = edge.next
      end
   end
end
function latticemt:random_path()
   local path = ffi.cast("LatticePath*", ffi.C.malloc(ffi.sizeof("LatticePath")))
   path.size =1
   path.maxsize = self.size
   path.path = ffi.cast("LatticeNode**", ffi.C.malloc(ffi.sizeof("LatticeNode*")*path.maxsize))
   path.edge = ffi.cast("LatticeEdge**", ffi.C.malloc(ffi.sizeof("LatticeEdge*")*path.maxsize))
   
   node = self.node[self.size-1]
   --counting nodes
   local nbNode = 0
   while node~=nil do
      nbNode = nbNode + 1
      node = node.next
   end
   --choosing a random node
   nNode = math.random(nbNode)
   node = self.node[self.size-1]
   for i=1,nNode-1 do
      node = node.next			     
   end
   path.path[0] = node
   local count = 0
   while node.col~=1 do
      --counting edges
      edge = node.edge
      local nbEdge = 0
      while edge~=nil do
	 nbEdge = nbEdge + 1
	 edge = edge.next
      end
      --choosing a random edge 
      nEdge = math.random(nbEdge)
      edge = node.edge
      for i=1,nEdge-1 do
	 edge = edge.next
      end
      path.edge[count] = edge
      count = count + 1
      path.size = path.size + 1
      node = edge.src_node
      path.path[count] = node
   end
   
   return path
end

ffi.cdef[[
float lattice_forward_logadd(LatticeNode** nodes, int latticesize, int sz);
]]
local clib = ffi.load('/idiap/home/jlegrand/newtorch/lib/liblattice_kbest.so')
--local clib = ffi.load('/idiap/home/jlegrand/historique/2014/03-02-14_package_lattice/lattice/liblattice_kbest.so')
function latticemt:forward_logadd(sz)
   if true then
      return clib.lattice_forward_logadd(self.node, self.size, sz or self.size)
   end
   sz = sz or self.size
   local startNode = self.size - sz   
   local node = self.node[startNode]
   while node~=nil do
      node.max_edge[0] = nil
      node.accScore[0] = node.score[0]
      node = node.next
   end
   NEXP = 0
   for t=startNode+1,self.size-1 do
      node = self.node[t]
      while node~=nil do
	 --finding max edge
	 local edge = node.edge
	 local max_edge = nil
	 local max_score = -math.huge;
	 while edge~=nil do
	    if edge.score[0] + edge.src_node.accScore[0] > max_score and edge.src_node.col>startNode then
	       max_score = edge.score[0] + edge.src_node.accScore[0]
	       max_edge = edge
	    end
	    edge = edge.next
         end
	 node.max_edge[0] = max_edge;
	 --summing scores
	 local sum = 0
	 edge = node.edge
--	 while edge~=nil do
--	    if edge.src_node.col > startNode then
	 --	       sum = sum + expm((max_score - edge.src_node.accScore[0] - edge.score[0]))
--	    end
--	    edge = edge.next
--	 end
	 sum = max_score + math.abs(sum) + node.score[0]
	 node.accScore[0] = node.score[0] + sum
	 node = node.next
      end
   end  
   
   --find the best end-node
   local maxendnode = nil
   local maxscore = -math.huge
   node = self.node[self.size-1]
   while node~=nil do
      if node.accScore[0] > maxscore then
	 maxscore = node.accScore[0]
	 maxendnode = node
      end
      node = node.next
   end

   print("forward_logadd " .. maxscore)

   --return the best path
   node = maxendnode
   self.path.size = 0
   while node~=nil do
      self.path.path[self.path.size] = node
      if node.max_edge[0]~=nil then
	 self.path.edge[self.path.size] = node.max_edge[0]
	 node = node.max_edge[0].src_node
      else
	 node = nil
      end
      self.path.size = self.path.size + 1 --tester si path assez long -> realloc self.path.path 
   end
   
   local latticeScore = maxscore;
   node = self.node[self.size-1];
--    while node~=nil do
   --       latticeScore = LogAdd(latticeScore, node.accScore[0]);
--       node = node.next;
   --    end
   return latticeScore, self.path
end

ffi.cdef[[
void lattice_backward_logadd(LatticeNode** nodes, int latticesize, int sz, float g);
]]

function latticemt:backward_logadd(g, sz)
   if true then
      return clib.lattice_backward_logadd(self.node, self.size, sz or self.size, g)
   end
   sz = sz or self.size
   local startNode = self.size - sz --hop
   sum = 0;
   max_score = -math.huge;
   
   --finding max_score
   node = self.node[self.size-1]
   while node~=nil do
      if node.accScore[0]>max_score then
	 max_score = node.accScore[0]
      end
      node = node.next
   end
   
   node = self.node[self.size-1]
   while node~=nil do
      node.accGradScore = expm((max_score - node.accScore[0]))
      if node.accGradScore~=node.accGradScore then error("error1") end
      sum = sum + node.accGradScore
      node = node.next
   end
   node = self.node[self.size-1]
   
   while node~=nil do
      node.accGradScore = node.accGradScore/sum
      if node.accGradScore~=node.accGradScore then
	 error("error1") 
      end

      node = node.next
   end
   
   --zero
   for t=self.size-1,startNode,-1 do
      node = self.node[t]
      while node~=nil do
	 edge = node.edge;
	 while edge~=nil do
	    edge.src_node.accGradScore = 0;
	    edge = edge.next
	 end
	 node = node.next;
      end
   end
   
   for t=self.size-1,startNode+1,-1 do
      node = self.node[t];
      while node~=nil do
	 --if node.idx==513 then print(node.gradScore[0] .. " " .. g*node.accGradScore) end
	 node.gradScore[0] = node.gradScore[0] + g*node.accGradScore;
	 sum = 0;

	 ------------------old version but more efficient--------------------
	 max_score = node.max_edge[0].score[0] + node.max_edge[0].src_node.accScore[0]
	 edge = node.edge;
	 
--	 local st = ""
		 while edge~=nil do
	    if edge.src_node.col>startNode then
--	       st = st .. " " .. edge.src_node.accScore[0] + edge.score[0] .. "(" .. edge.src_node.col .. " " .. edge.src_node.idx .. ")"
	       edge.accEdge = expm((max_score - edge.src_node.accScore[0] - edge.score[0]));
	       sum = sum + edge.accEdge;
	    end
	    edge = edge.next;
	 end
	 
	 edge = node.edge;
	 while edge~=nil do
	    if edge.src_node.col>startNode then
	       local back = edge.accEdge
	       edge.accEdge = edge.accEdge / sum
	      
	       --if edge.accEdge~=edge.accEdge then
		  --printLattice(self)
		  --print("accEdge / sum : " .. back .. " / " .. sum .. " = nan")
		  --print("startnode : " .. startNode)
		  --print("node " .. node.col .. " " .. node.idx)
		  --print("max : " .. max_score .. "(" .. node.max_edge[0].src_node.col .. " " .. node.max_edge[0].src_node.idx .. ")")
		  --print(st)
		  
	       --error("accEdge is nan")
	       --end
	    end
	    edge = edge.next;
	 end

	 ---------------new version test-------------------------------------
	 -- edge = node.edge
	 -- while edge~=nil do
	 --    if edge.src_node.col>startNode then
	 --       local tempedge = node.edge
	 --       local sum = 0
	 --       while tempedge~=nil do
	 -- 	  if edge.src_node.idx==tempedge.src_node.idx 
	 -- 	     and edge.src_node.col==tempedge.src_node.col then
	 -- 	     --print("same")
	 -- 	  else
	 -- 	     --print("add")
	 -- 	     sum = sum + math.exp(tempedge.src_node.accScore[0] 
	 -- 				  + tempedge.score[0] - edge.src_node.accScore[0] - edge.score[0])
	 -- 	  end
	 -- 	  tempedge = tempedge.next
	 --       end
	 --       edge.accEdge = 1 / (1 + sum)
	 --    end
	 --    edge = edge.next
	 -- end

	 edge = node.edge;
	 while edge~=nil do
	    if edge.src_node.col>startNode then
	       edge.src_node.accGradScore = edge.src_node.accGradScore + node.accGradScore*edge.accEdge
	       if edge.src_node.accGradScore~=edge.src_node.accGradScore then error("edge.src_node.accGradScore is nan") end
	       edge.gradScore[0] = edge.gradScore[0] + edge.accEdge * g*node.accGradScore;
	    end
	    edge = edge.next;
	 end
	 node = node.next;
      end
   end
   
   node = self.node[startNode];
   while node~=nil do
      node.gradScore[0] = node.gradScore[0] + g*node.accGradScore;
      node = node.next;
   end
end	
function latticemt:nbNodes()
   return nbNodes()
end
function latticemt:nbEdges()
   return nbEdges()
end
function latticemt:printLattice(file, full) --TO-DO : regroup nodes
   f = io.open(file, "w")
   f:write("digraph G {\nrankdir=LR\n\nnode [style=solid,color=blue1, shape=circle];\n")
   --nodes
   for i=0,self.size-1 do
      local link = ""
      local rank = "{rank=same; "
      node = self.node[i]
      while node~=nil do
	 link = link .. "x" .. node.col .. "_" .. node.idx
	 rank = rank .. "x" .. node.col .. "_" .. node.idx .. " "
	 if node.next~= nil then link = link .. " -> " end
	 f:write("x" .. node.col .. "_" .. node.idx .. " [label=x" .. node.col .. "_" .. node.idx .. "]\n")
	 node = node.next
      end
      rank = rank .. "}"
      f:write(rank .. "\n")
      f:write(link .. "\n")
   end
   --edges
   print("edges")
   for i=1,self.size-1 do
      node = self.node[i]
      edge = node.edge
      while edge~=nil do
	 f:write("x" .. edge.src_node.col .. "_" .. edge.src_node.idx .. " -> x" .. node.col .. "_" .. node.idx  .. "\n")
	 edge = edge.next
	 
      end     
   end   
   f:write("}")
end

function latticemt:toString(sz)
   sz = sz or self.size
   local startNode = self.size - sz --hop
   
   print("size " .. self.size)

   local compteurEdge = 0
   local compteurNode = 0
   for i=1+startNode,self.size do
      --print(i-1)
      print("colonne " .. i)
      local node = self.node[i-1]
      while node~=nil do
	 compteurNode = compteurNode + 1
	 if i==1 then
	    print("node " .. node.col .. " " .. node.idx .. "s(" .. node.score[0] .. ") gs(" .. node.gradScore[0] .. ")  as(" .. node.accScore[0] .. " [" .. node.max_edge_indice[0] .. "] " .. node.accScore[1] .. " [" .. node.max_edge_indice[1] .. "])")
	 else
	    print("node " .. node.col .. " " .. node.idx .. "s(" .. node.score[0] .. ") gs(" .. node.gradScore[0] .. ")  as(" .. node.accScore[0] .. " [n" .. ((node.max_edge[0]~=nil and node.max_edge[0].src_node.idx) or "-1") .. "s" .. node.max_edge_indice[0] .. "] " .. node.accScore[1] .. " [n" .. ((node.max_edge[1]~=nil and node.max_edge[1].src_node.idx) or "-1") .. "s" .. node.max_edge_indice[1] .. "])")
	 end
	 local edge = node.edge
	 while edge~=nil do
	    compteurEdge = compteurEdge + 1
	    print("\t<- " .. edge.src_node.col .. " " .. edge.src_node.idx .. "(" .. edge.score[0] .. ") (" .. edge.gradScore[0] .. ")")
	    edge = edge.next
	 end
	 node = node.next
	 --print(node)
      end
   end
end

function Lattice.new(T, N, f_node, f_edge)
   --local lattice = ffi.cast("Lattice*", ffi.C.malloc(ffi.sizeof("Lattice")))
   local lattice = {}
   lattice.size = T
   lattice.nbTag = N
   local N = N
  
   lattice.kbest = 10

   lattice.node  = ffi.cast("LatticeNode**", ffi.C.malloc(ffi.sizeof("LatticeNode*")*T))
   for i=1,T do lattice.node[i-1] = nil end
   

   local tabNodes = ffi.cast("LatticeNode**", ffi.C.malloc(ffi.sizeof("LatticeNode*")*T*N))
   
   --creating nodes
   local currentNode;
   local currenti = -1
   for i, j, ptr, grad  in f_node() do
      if (i>T or j>N) then error("node indice incorrect in f_node " .. i .. " " .. j) end
      --print("nodes : " .. i .. " " .. j)
      if currenti ~= i-1 then
	 currenti = i-1
	 currentNode = getNode()
	 currentNode.col = i
	 currentNode.idx = j
	 currentNode.score = ptr
	 currentNode.gradScore = grad
	 currentNode.next = nil
	 currentNode.accScore = Lattice.getPFloat(lattice.kbest)
	 currentNode.max_edge_indice = Lattice.getPInt(lattice.kbest)
	 currentNode.max_edge = Lattice.getPEdge(lattice.kbest)
	 lattice.node[currenti] = currentNode 
	 
      else
	 local node = getNode()
	 node.col = i
	 node.idx = j
	 node.score = ptr
	 node.gradScore = grad
	 node.next = nil
	 node.accScore = Lattice.getPFloat(lattice.kbest)
	 node.max_edge_indice = Lattice.getPInt(lattice.kbest)
	 node.max_edge = Lattice.getPEdge(lattice.kbest)
	 currentNode.next = node
	 currentNode = currentNode.next
      end
      tabNodes[((i-1)*N) + (j-1)] = currentNode
   end
   


   --creating edges
   local currentEdge
   local tempEdge
   local currentNode
   for i,j,i_,j_, ptr, grad in f_edge() do
      --io.write("")
      --print("machin")
      --print(i_ .. " " .. j_ .. " " .. i .. " " .. j)
      if (i>T or j>N or i_>T or j_>N) then 
	 --print("machin")
	 error("node indice incorrect in f_edge")
      end
      if currentNode~=nil and currentNode.col == i and currentNode.idx == j then
	 --print("toto")
	 tempEdge = getEdge()
	 tempEdge.src_node = tabNodes[((i_-1)*N) + (j_-1)]
	 tempEdge.score = ptr
	 tempEdge.gradScore = grad
	 tempEdge.next = nil
	 tempEdge.size = i-i_
	 currentEdge.next = tempEdge
	 currentEdge = currentEdge.next
      else
	 --print("titi")
	 currentNode = tabNodes[((i-1)*N) + (j-1)]
	 --print("hop")
	 
	 currentEdge = getEdge()
	 --print(nbEdges())
	 --print(currentEdge)
	 currentEdge.score = ptr
	 currentEdge.gradScore = grad
	 --print(currentEdge)
	 currentEdge.src_node = tabNodes[((i_-1)*N) + (j_-1)]
	 --print("hop")
	 currentEdge.next = nil
	 currentEdge.size = i-i_
	 --print(currentNode)
	 currentNode.edge = currentEdge
	 --print("lala")
      end
      --print("next")
      --io.read()
   end
   
   lattice.tabNodes = tabNodes

   lattice.path = ffi.cast("LatticePath*", ffi.C.malloc(ffi.sizeof("LatticePath")))
   lattice.path.size =0
   lattice.path.maxsize = 10000
   lattice.path.path = ffi.cast("LatticeNode**", ffi.C.malloc(ffi.sizeof("LatticeNode*")*lattice.path.maxsize))
   lattice.path.edge = ffi.cast("LatticeEdge**", ffi.C.malloc(ffi.sizeof("LatticeEdge*")*lattice.path.maxsize))	
   
   setmetatable(lattice, {__index=latticemt})

   return lattice
end

return Lattice
