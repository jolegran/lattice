local ffi = require "ffi"

ffi.cdef[[
      typedef struct LatticeNode_
      {
	 struct LatticeNode_ *next;
	 struct LatticeEdge_ *edge;
	 struct LatticeEdge_ *max_edge;
	 
	 double *score;
	 double *gradScore;
	 double accScore;
	 double accGradScore;
	 
	 int idx;
	 int col;
      } LatticeNode;
      
      typedef struct LatticeEdge_
      {
	 struct LatticeEdge_ *next;
	 struct LatticeNode_ *src_node;
	 
	 double *score;
	 double *gradScore;
	 double accEdge;
	 int size;
	 
      } LatticeEdge;
      
      typedef struct LatticePath_
      {
	 int maxsize;
	 int size;
	 LatticeNode **path;
	 LatticeEdge **edge;
      } LatticePath;

      typedef struct
      {
	 LatticeNode **node;
	 int size;
      } Lattice;
]]
